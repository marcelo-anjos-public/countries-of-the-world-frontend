#!/usr/bin/env bash

echo "Starting Raketech Frontend Developer Test"

docker compose up -d
docker exec -it raketech_frontend  /bin/bash
