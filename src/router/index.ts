import { createRouter, createWebHashHistory, Router } from "vue-router";
import Home from "../views/Home.vue";
import { App } from 'vue';
import { createAuthGuard } from "@auth0/auth0-vue";

export default function router(app: App): Router {
    return createRouter({
        routes: [
            {
                path: "/",
                name: "home",
                component: Home,
                beforeEnter: createAuthGuard(app)
            }
        ],
        history: createWebHashHistory()
    })
}