# RakeTech Countries - Code Challenge

This project intends to demonstrate my code skills.

## Requirements
   - [Docker Desktop](https://www.docker.com/products/docker-desktop) 

## Installation

1. Clone the repository
2. Create `auth_config.json` file in the root of the project with the following content:
```json
  {
      "domain": "<YOUR_AUTH0_DOMAIN>",
      "clientId": "<YOUR_AUTH0_CLIENT_ID>"
  }
   ```
    
3. Create `api_config.json` file in the root of the project with the following content:
```json
      {
          "apiKey": "<YOUR_API_KEY>",
          "apiUrl": "<YOUR_API_URL>"
      }
   ```

4. Run `sh init.sh` to start the development server
5. Run `npm install` to install the dependencies
6. Run `npm run dev` to start the development server 
7. Open your browser and go to `http://localhost:3000`

## Configuration
The `apiKey` defined on `api_config.json` must be valid Api Key defined on `.env` file on the backend project. \
The `apiUrl` defined on `api_config.json` must be the backend project url.

> [!WARNING]
> Not intended for production, and it is not production ready.